package week5.domain;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Hobby extends Person{
    private int frequency;
    private String hobbyName;
    private final List<Adress> adresses = new ArrayList<>();

    public Hobby(int frequency, String hobbyName) {
        super();
        this.frequency = frequency;
        this.hobbyName = hobbyName;
    }
    void addAdress(String country, String city) {
        adresses.add(new Adress(country, city));
    }
    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getHobbyName() {
        return hobbyName;
    }

    public void setHobbyName(String hobbyName) {
        this.hobbyName = hobbyName;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "frequency=" + frequency +
                ", hobbyName='" + hobbyName + '\'' +
                ", adresses=" + adresses +
                '}';
    }
}

