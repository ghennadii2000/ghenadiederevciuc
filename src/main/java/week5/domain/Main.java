package week5.domain;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello");

        ////////////////////Add persons
        Person person1 = new Student(19,"Ioan");
        Person person2 = new Employee(45,"Vasile");
        Person person3 = new Unemployed(29,"Aurica");

        //////////////////////Store the persons in a TreeSet and define the first comparator - sort by name
        ///////////////////// using NameComparator class
        TreeSet<Person> personSortByName = new TreeSet<Person>(new NameComparator());

        personSortByName.add(person1);
        personSortByName.add(person2);
        personSortByName.add(person3);

        System.out.println("\nPrinting comparator sorted by name in ascending order:");
        ///////////////// Iterate and print by name
       for(Person personName : personSortByName){
            System.out.println(personName);
       }
        //////////////////////Store the persons in a TreeSet and define the second comparator - sort by age
        ////////////////////// using AgeComparator class
        TreeSet<Person> personSortByAge = new TreeSet<Person>(new AgeComparator());

        personSortByAge.add(person1);
        personSortByAge.add(person2);
        personSortByAge.add(person3);


        System.out.println("\nPrinting comparator sorted by age in ascending order:");
        ///////////////// Iterate and print by name
        for(Person personAge : personSortByAge){
           System.out.println(personAge);
       }
        //////////////////// Add hobies to the list
        Hobby hobby1 = new Hobby(4,"cycling");
        Hobby hobby2 = new Hobby(2,"swimming");
        Hobby hobby3 = new Hobby(3,"running");

        ////////////// Add adresses to the hobies
        hobby1.addAdress("USA","NY");
        hobby2.addAdress("Spain","Barcelona");
        hobby3.addAdress("Romania","Bucharest");


        System.out.println("\nHobbies are:");
        List<Hobby> hobbies = new ArrayList<>();
        hobbies.add(hobby1);
        hobbies.add(hobby2);
        hobbies.add(hobby3);
        for(Hobby hobby : hobbies){
            System.out.println(hobby);
        }


        ///////////////////// Use a HashMap with the following structure <Person, List<Hobby>>
        System.out.println("\nMap collection:");
        Map<Person,List<Hobby>> personListMap = new HashMap<>();
        personListMap.put(person1, Collections.singletonList(hobby1));
        personListMap.put(person2, Collections.singletonList(hobby2));
        personListMap.put(person3,  Collections.singletonList(hobby3));
        for(Map.Entry<Person,List<Hobby>> entry : personListMap.entrySet()){
            Person key = entry.getKey();
            List<Hobby> values = entry.getValue();
            System.out.println("Key: "+key+" Values: "+values);
        }

    }
}
