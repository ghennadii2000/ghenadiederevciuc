package week4.domain;

import week4.domain.Car;
import week4.domain.Golf;
import week4.domain.Logan;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world");

        Car car = new Logan(27, "oiqe0934hkkadsn");

        car.start();

        car.shiftGear(1);

        car.drive(0.01f);

        car.shiftGear(2);

        car.drive(0.02f);

        car.shiftGear(3);

        car.drive(0.5f);

        car.shiftGear(4);

        car.drive(0.5f);

        car.shiftGear(4);

        car.drive(0.5f);

        car.shiftGear(5);

        car.drive(10);

        car.shiftGear(4);

        car.drive(0.5f);

        car.shiftGear(3);

        car.drive(0.1f);

        car.stop();
        float availableFuel = car.getAvailableFuel();

        float fuelConsumedPer100Km = car.getAverageFuelConsumption();


        System.out.println("Available fuel: " + availableFuel);
        System.out.println("Average fuel consumption: " + fuelConsumedPer100Km);

        Car car1 = new Golf(30, "1987ddkshik289");

        car1.start();

        car1.drive(1);

        car1.stop();

        //Car car1 = (Car) vehicle;

        float availableFuel1 = car1.getAvailableFuel();

        float fuelConsumedPer100Km1 = car1.getAverageFuelConsumption();



        System.out.println("Available fuel: " + availableFuel1);
        System.out.println("Average session fuel consumption: " + fuelConsumedPer100Km1);

    }
}