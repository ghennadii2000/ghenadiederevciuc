package week4.domain;

public abstract class Volkswagen extends Car {
    public Volkswagen() {
        super();
        manufacturerName = "VAG - Volkswagen Audi Group GmbH";

    }
}
