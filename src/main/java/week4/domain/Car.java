package week4.domain;

public abstract class Car implements Vehicle  {
    protected String manufacturerName;
    protected String name;
    protected String chassisNumber;
    protected int tankSize;
    protected String fuelType;
    protected static final String[] FUEL_TYPES = { "PETROL", "DIESEL", "HYBRID", "ELECTRIC" };
    protected int numOfGears = 6;
    protected float averageFuelConsumption;
    protected float availableFuel;
    protected float distance;
    protected float consumption;
    protected int tireSize ;
    //protected static final String[] TIRE_SIZE = { "15", "16", "17", "18" };
    protected int currentGear = NEUTRAL;
    protected static final int FIRST = 1, SECOND = 2, THIRD = 3, FOURTH = 4, FIFTH = 5, NEUTRAL = 0, REVERSE = -1;
    protected boolean started = false;


    public Car() {
    }


    public void shiftGear(int gear) {
        if ((gear < this.numOfGears) && (gear >= REVERSE)) {
            this.currentGear = gear;
           System.out.println("Changing gear...\nCurrent gear is: " + this.currentGear);
        } else {
            System.out.println("Error: Invalid gear. Current gear is: " + this.currentGear);
            System.out.println("Exiting app...");

        }

    }
    public float getAvailableFuel() {
        return this.availableFuel;
    }

    protected abstract float consume(float distance);

    public float getAverageFuelConsumption() {
        if (consumption != 0) {
            return consumption / distance * 100;
        } else {
            System.out.println("Car has not consumed anything.");
            return 0f;
        }

    }

    @Override
    public String toString() {
        return "Car{" +
                "manufacturerName='" + manufacturerName + '\'' +
                ", name='" + name + '\'' +
                ", tankSize=" + tankSize +
                ", fuelType='" + fuelType + '\'' +
                ", numOfGears=" + numOfGears +
                ", averageFuelConsumption=" + averageFuelConsumption +
                ", availableFuel=" + availableFuel +
                ", chassisNumber='" + chassisNumber + '\'' +
                ", distance=" + distance +
                ", consumption=" + consumption +
                ", tireSize=" + tireSize +
                ", currentGear=" + currentGear +
                ", started=" + started +
                '}';
    }
}
