package week4.domain;

public interface Vehicle {
    public void start();
    public void drive(float distance);
    public void stop();
}
