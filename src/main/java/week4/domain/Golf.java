package week4.domain;

public class Golf extends Volkswagen {
    public Golf() {
        super();
        this.name="Golf";
        this.fuelType = FUEL_TYPES[1];
        this.averageFuelConsumption = 4.5f;
        this.tankSize = 60;
        this.tireSize = 16;
    }

    public Golf(int availableFuel, String chassisNumber) {
        this();
        this.availableFuel = availableFuel;
        this.chassisNumber = chassisNumber;
    }

    @Override
    public void start() {
        this.distance = 0;
        this.consumption = 0;
        this.started = true;
        System.out.println(this.toString() + " started.");
    }

    @Override
    public void drive(float distance) {
        float consumedFuel;
        consumedFuel = consume(distance);
        this.distance += distance;
        this.consumption += consumedFuel;
        this.availableFuel -= consumedFuel;
    }

    @Override
    public void stop() {
        this.started = false;
        System.out.println(this.toString() + " stopped.");

    }

    protected float consume(float distance) {
        float amountConsumed = distance / 100 * averageFuelConsumption;
        switch (this.currentGear) {
            case NEUTRAL:
                return 0f;
            case REVERSE:
                return (float) (amountConsumed * 2.2);
            case FIRST:
                return (float) (amountConsumed * 2.2);
            case SECOND:
                if(tireSize>15){
                    return (float) (amountConsumed * 2.1);}
                else {return (float) (amountConsumed * 1.8);}
            case THIRD:
                if(tireSize>15){
                    return (float) (amountConsumed * 1.9);}
                else {return (float) (amountConsumed * 1.5);}
            case FOURTH:
                if(tireSize>15){
                    return (float) (amountConsumed * 1.7);}
                else {return (float) (amountConsumed * 1.2);}
            case FIFTH:
                if(tireSize>15){
                    return (float) (amountConsumed * 1);}
                else {return (float) (amountConsumed);}
            default:
                return -1;
        }
    }


}
